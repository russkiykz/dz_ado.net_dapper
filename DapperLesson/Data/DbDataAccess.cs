﻿using DapperLesson.Services;
using System;
using System.Collections.Generic;
using System.Data.Common;

namespace DapperLesson.Data
{
    public abstract class DbDataAccess<T> : IDisposable
    {
        protected readonly DbProviderFactory factory;
        protected readonly DbConnection sqlConnection;

        public DbDataAccess()
        {
            factory = DbProviderFactories.GetFactory("DapperSqlClient");
            sqlConnection = factory.CreateConnection();
            sqlConnection.ConnectionString = ConnectionService.Configuration["dataConnectionString"];
            sqlConnection.Open();
        }

        public abstract void Insert(T entity);
        public abstract void Update(T entity);
        public abstract void Delete(T entity);
        public abstract ICollection<T> Select();

        public void Dispose()
        {
            sqlConnection.Close();
        }
    }
}
