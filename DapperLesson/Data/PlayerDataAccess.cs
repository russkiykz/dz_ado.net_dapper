﻿using Dapper;
using DapperLesson.Models;
using System.Collections.Generic;
using System.Linq;

namespace DapperLesson.Data
{
    public class PlayerDataAccess : DbDataAccess<Player>
    {
        public override void Delete(Player entity)
        {
            using (var connection = sqlConnection)
            {
                connection.Execute("delete from Players where Id=@Id", entity);
            }
        }

        public override void Insert(Player entity)
        {
            using (var connection = sqlConnection)
            {
                connection.Execute("insert into Players (Id,FullName,Number,TeamId) values (@Id, @FullName,@Number,@TeamId)", entity);
            }
        }

        public override ICollection<Player> Select()
        {
            using (var connection = sqlConnection)
            {
                return connection.Query<Player>("select * from Players").ToList();
            }
        }

        public override void Update(Player entity)
        {
            using (var connection = sqlConnection)
            {
                connection.Execute("update Players set FullName=@FullName,Number=@Number,TeamId=@TeamId where Id=@Id", entity);
            }
        }
    }
}
