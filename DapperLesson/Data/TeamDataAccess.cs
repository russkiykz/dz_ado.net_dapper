﻿using Dapper;
using DapperLesson.Models;
using System.Collections.Generic;
using System.Linq;

namespace DapperLesson.Data
{
    public class TeamDataAccess : DbDataAccess<Team>
    {
        public override void Insert(Team entity)
        {
            using (var connection = sqlConnection)
            {
                connection.Execute("insert into Teams (Id,Name) values (@Id, @Name)", entity);
            }
        }
        public override void Update(Team entity)
        {
            using (var connection = sqlConnection)
            {
                connection.Execute("update Teams set Name=@Name where Id=@Id", entity);
            }
        }
        public override void Delete(Team entity)
        {
            using (var connection = sqlConnection)
            {
                connection.Execute("delete from Teams where Id=@Id", entity);
            }
        }
        public override ICollection<Team> Select()
        {
            using (var connection = sqlConnection)
            {
                return connection.Query<Team>("select * from Teams").ToList();
            }
        }
    }
}
