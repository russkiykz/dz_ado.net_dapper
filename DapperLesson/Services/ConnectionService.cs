﻿using Microsoft.Extensions.Configuration;
using System.Data.Common;
using System.Data.SqlClient;

namespace DapperLesson.Services
{
    public class ConnectionService
    {
        public static IConfiguration Configuration { get; private set; }
        public static void Init()
        {
            DbProviderFactories.RegisterFactory("DapperSqlClient", SqlClientFactory.Instance);
            if (Configuration == null)
            {
                var configurationBuilder = new ConfigurationBuilder();
                Configuration = configurationBuilder.AddJsonFile("appSettings.json").Build();
            }
        }
    }
}
